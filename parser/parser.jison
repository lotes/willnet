%lex
%%

\s+                   ; /* skip whitespace */
"//"[^\n]*            ;
[0-9]+\b              return 'INTEGER_LITERAL';
[0-9]+("."[0-9]+)?\b  return 'FLOAT_LITERAL';
\"(\\.|[^\\\"])*\"    return 'STRING_LITERAL';
"*"                   return '*';
"/"                   return '/';
"-"                   return '-';
"+"                   return '+';
"^"                   return '^';
"%"                   return '%';
"("                   return '(';
")"                   return ')';
","                   return ',';
";"                   return ';';
"["                   return '[';
"]"                   return ']';
"{"                   return '{';
"}"                   return '}';
":="                  return ':=';
":"                   return ':';
"="                   return '=';
"<>"                  return '<>';
"<="                  return '<=';
">="                  return '>=';
"<"                   return '<';
">"                   return '>';
[aA][nN][aA][uU][sS]\b                              return 'BOOLEAN';
[rR][eE][fF]\b                                      return 'REF';
[vV][aA][rR]\b                                      return 'VAR';
[gG][aA][nN][zZ][zZ][aA][hH][lL]\b                  return 'INTEGER';
[dD][eE][zZ][iI][mM][aA][lL][zZ][aA][hH][lL]\b      return 'FLOAT';
[zZ][eE][iI][cC][hH][eE][nN][kK][eE][tT][tT][eE]\b  return 'STRING';
[fF][uU][nN][kK][tT][iI][oO][nN]\b                  return 'FUNCTION';
[nN][iI][cC][hH][tT]\b                              return 'NOT';
[uU][nN][dD]\b                                      return 'AND';
[oO][dD][eE][rR]\b                                  return 'OR';
[wW][eE][nN][nN]\b                                  return 'IF';
[sS][oO][nN][sS][tT]\b                              return 'ELSE';
[aA][uU][sS][gG][aA][bB][eE]\b                      return 'RETURN';
[sS][oO][lL][aA][nN][gG][eE]\b                      return 'WHILE';
[wW][iI][eE][dD][eE][rR][hH][oO][lL][eE]\b          return 'DO';
([aA][nN]|[aA][uU][sS])\b                           return 'BOOLEAN_LITERAL';
[a-zA-Z_][a-zA-Z_0-9]*(\.[a-zA-Z_][a-zA-Z_0-9])*\b  return 'ID';
<<EOF>>               return 'EOF';
.                     return 'INVALID';

/lex

/* operator associations and precedence */
%left '=' '<>'
%left '<=' '>=' '<' '>'
%left OR
%left AND
%left '+' '-'
%left '*' '/' '%'
%right '^'
%left NOT UMINUS
%right ELSE THEN

%start translationUnit

%% /* language grammar */

translationUnit
    : statements EOF {
      return {
        translationUnit: "STATEMENTS",
        statements: $1
      };
    }
    | ID EOF {
      return {
        translationUnit: "ID",
        id: $1
      };
    }
    ;

definitions
    : definition definitions { $$ = [$1].concat($2); }
    | definition { $$ = [$1]; }
    ;
    
definition
    : functionDefinition { $$ = $1; }
    ;
    
functionDefinition
    : FUNCTION ID parethizedFormalParameters ':' type statementBlock { 
      $$ = {
        definitionType: 'FUNCTION',
        name: $2,
        parameters: $3,
        returnType: $5,
        body: $6,
        position: { line: @2.first_line, column: @2.first_column }
      };
    }
    | FUNCTION ID parethizedFormalParameters statementBlock { 
      $$ = {
        definitionType: 'FUNCTION',
        name: $2,
        parameters: $3,
        returnType: null,
        body: $4,
        position: { line: @2.first_line, column: @2.first_column }
      };
    }
    ;
    
parethizedFormalParameters
    : '(' ')' { $$ = []; }
    | '(' formalParameters ')' { $$ = $2; }
    ;
    
formalParameters
    : formalParameter ',' formalParameters { $$ = [$1].concat($3); }
    | formalParameter { $$ = [$1]; }
    ;
    
formalParameter
    : ID ':' type { 
      $$ = {
        isRef: false,
        name: $1,
        type: $3,
        position: { line: @1.first_line, column: @1.first_column }
      }; 
    }
    | REF ID ':' type { 
      $$ = {
        isRef: true,
        name: $1,
        type: $3,
        position: { line: @2.first_line, column: @2.first_column }
      }; 
    }
    ;
   
type
    : baseType { 
      $$ = { 
        base: $1, 
        array: -1,
        position: { line: @1.first_line, column: @1.first_column }
      }; 
    }
    | baseType '[' INTEGER_LITERAL ']' { 
      $$ = { 
        base: $1, 
        array: parseInt($3),
        position: { line: @1.first_line, column: @1.first_column }
      }; 
    }
    ;
    
baseType
    : BOOLEAN { $$ = 'BOOLEAN'; }
    | INTEGER { $$ = 'INTEGER'; }
    | FLOAT { $$ = 'FLOAT'; }
    | STRING { $$ = 'STRING'; }
    ;
    
statementBlock
    : '{' '}' { $$ = []; }
    | '{' statements '}' { $$ = $2; }
    ;
    
statements
    : statement statements { $$ = [$1].concat($2); }
    | statement { $$ = [$1]; }
    ;
    
statement
    : statementBlock { $$ = { statementType: "BLOCK", statements: $1 }; }
    | ID ':=' expr ';' {
      $$ = {
        statementType: "ASSIGNMENT",
        variable: $1,
        value: $3,
        position: { line: @2.first_line, column: @2.first_column }
      };
    }
    | VAR ID '=' expr ';' {
      $$ = {
        statementType: "INITIALIZATION",
        variable: $2,
        value: $4,
        position: { line: @1.first_line, column: @1.first_column }
      };
    }
    | expr ';' {
      $$ = {
        statementType: "EXPRESSION",
        expression: $1,
        position: { line: @1.first_line, column: @1.first_column }
      };
    }
    | RETURN expr ';' {
      $$ = {
        statementType: "RETURN",
        expression: $2,
        position: { line: @1.first_line, column: @1.first_column }
      };
    }
    | IF '(' expr ')' statement %prec THEN {
      $$ = {
        statementType: "IF",
        condition: $3,
        thenBranch: $5,
        position: { line: @1.first_line, column: @1.first_column }
      };
    }
    | IF '(' expr ')' statement ELSE statement {
      $$ = {
        statementType: "IFELSE",
        condition: $3,
        thenBranch: $5,
        elseBranch: $7,
        position: { line: @1.first_line, column: @1.first_column }
      };
    }
    | WHILE '(' expr ')' statement {
      $$ = {
        statementType: "WHILE",
        condition: $3,
        statements: $5,
        position: { line: @1.first_line, column: @1.first_column }
      };
    }
    | DO statement WHILE '(' expr ')' ';' {
      $$ = {
        statementType: "DO",
        condition: $5,
        statements: $2,
        position: { line: @1.first_line, column: @1.first_column }
      };
    }
    ;
expr
    : expr '+' expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: '+',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | expr '-' expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: '-',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | expr AND expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: 'AND',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | expr 'OR' expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: 'OR',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | expr '*' expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: '*',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | expr '/' expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: '/',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | expr '^' expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: '^',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | NOT expr
        {
          $$ = {
            expressionType: 'UNARY_OPERATION',
            operator: '!',
            operand: $2,
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    | expr '%' expr
        {
          $$ = {
            expressionType: 'BINARY_OPERATION',
            operator: '%',
            leftOperand: $1,
            rightOperand: $3,
            position: { line: @2.first_line, column: @2.first_column }
          };
        }
    | '-' expr %prec UMINUS
        {
          $$ = {
            expressionType: 'UNARY_OPERATION',
            operator: '-',
            operand: $2,
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    | '(' expr ')'
        {$$ = $2;}
    | INTEGER_LITERAL
        {
          $$ = {
            expressionType: "INTEGER_LITERAL",
            value: parseInt(yytext),
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    | FLOAT_LITERAL
        {
          $$ = {
            expressionType: "FLOAT_LITERAL",
            value: parseFloat(yytext),
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    | BOOLEAN_LITERAL
        {
          $$ = {
            expressionType: "BOOLEAN_LITERAL",
            value: yytext.toLowerCase() == 'an',
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    | STRING_LITERAL
        {
          function parseString(str) {
            var length = str.length;
            var result = "";
            for(var srcIndex = 1; srcIndex < length - 1; srcIndex++) {
              var c = str[srcIndex];
              if(c == '\\') {
                srcIndex++;
                c = '';
                if(srcIndex < length - 1)
                  switch(str[srcIndex]) {
                    case 'n': c = '\n'; break;
                    case 'r': c = '\r'; break;
                    case 't': c = '\t'; break;
                    case '\"': c = '\"'; break;
                    case '\\': c = '\\'; break;
                  }
              }
              if(c == '') 
                throw new Error("Bad string format: "+str+"!");
              result += c;
            }
            return result;
          }
          $$ = {
            expressionType: "STRING_LITERAL",
            value: parseString(yytext),
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    | ID
        {
          $$ = {
            expressionType: "VARIABLE",
            name: $1,
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    | ID parenthizedActualParameters
        {
          $$ = {
            expressionType: "CALL",
            name: $1,
            parameters: $2,
            position: { line: @1.first_line, column: @1.first_column }
          };
        }
    ;
    
parenthizedActualParameters
    : '(' ')' { $$ = []; }
    | '(' actualParameters ')' { $$ = $2; }
    ;
    
actualParameters
    : expr ',' actualParameters { $$ = [$1].concat($3); }
    | expr { $$ = [$1]; }
    ;