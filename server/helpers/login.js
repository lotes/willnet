var Q = require('q');

module.exports = function (request, email, password) {
    var deferred = Q.defer();
    request
        .post('/auth/local')
        .send({
            email: email,
            password: password
        })
        .expect(200)
        .end(function (err, res) {
            if (err)
                deferred.reject(err);
            else
                deferred.resolve('Bearer '+res.body.token);
        });
    return deferred.promise;
};