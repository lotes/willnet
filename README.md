willnet
=======

Components
----------
-(1) sms: sending and receiving SMS via gammu process
-(2) phonebook: mapping of names to phone numbers
-(3) rfm69: socket connection to sensor nodes
-(4) parser: language for controlling node net
-(5) application: execution environment for code (programs, events)
-(6) logging: event log (time, node/module/event, value/object/list/json)
  -information retrieval and aggregation
-(7) debugger: test environment for code (programs, events) with breakpoints, stack trace, variable watching
-client: web frontend
  -SMS frontend
  -RFM69 frontend
  -Phonebook frontend
  -Application frontend
  -User frontend
  -Logging frontend
  -charts: visualization of logs (interactive SVG generation)
-server: web backend  
  -SMS web API (send/receive/mailboxes)
  -RFM69 web API (routing tables/socket connections/ping/netstat/rssi)
  -Phonebook web API (list/get/add/remove/edit entry)
  -Application web API (execute/list/get/add/remove/edit program/event)
  -Debugger web API (??? too much -> web sockets)
  -User web API (list/get/add/remove/edit user)
  -Logging web API